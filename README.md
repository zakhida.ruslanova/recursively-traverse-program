# Recursively Traverse Program

Create a console program in Java that can recursively traverse and download www.tretton37.com and save it to disk while keeping the online file structure. Show download progress in the console. 

# Planning

1) Use jsoup to connect  
2) Crawling on website
3) Methods for scanning of all folders and files with all levels
4) Downloading and saving on the local disk
5) Display the whole process
6) Handle e
7) Testing of all methods

What must I do to build my project architecture?
- model
  - Crawler-object 
  - Printer for displaying the process in console 
  - Task for threads handle
  - Loader for saving process
  - Executor which is creates before all threads are created
- service
  - classes of impl for handle crawling, file uploading 
  - interfaces for services
- exceptions
- tests

# where I got stuck 

1) InvalidPathException: Illegal char <:> at index 63 https // solution:  Path directory = Paths.get(currentWorkDir, path.replaceAll(":", ""));
2) java.net.MalformedURLException: unknown protocol: c // solution Used String instead URL
3) java.nio.file.InvalidPathException: Illegal char <?> at index 93:
