package se.ruza.rtp.main;

import se.ruza.rtp.model.Executor;
import se.ruza.rtp.model.TaskThreadsHandle;

public class Main {

    public static void main(String[] args) {

        Executor executor = new Executor();

        TaskThreadsHandle taskThreadsHandle = new TaskThreadsHandle(executor);
        for (int i = 0; i < taskThreadsHandle.getAmountCPU(); i++) {
            taskThreadsHandle.getService().execute(taskThreadsHandle);
        }
    }
}
// omstrukturera TaskThreadsHandle:
//      skapa alla instanser innan vi sätter igång trådar
//      spara alla misslyckades URL i try-catch
//      spara HTML, index.html
