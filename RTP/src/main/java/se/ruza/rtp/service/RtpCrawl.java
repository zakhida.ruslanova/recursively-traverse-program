package se.ruza.rtp.service;

import se.ruza.rtp.model.Crawler;

public interface RtpCrawl {

    void collectAndGoThrowAllLinks(Crawler crawler);

    void crawling(String url);
}
