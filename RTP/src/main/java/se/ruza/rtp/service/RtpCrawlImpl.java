package se.ruza.rtp.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Safelist;
import org.jsoup.select.Elements;
import se.ruza.rtp.model.Crawler;
import se.ruza.rtp.model.Loader;
import se.ruza.rtp.model.Printer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Set;

import static se.ruza.rtp.service.RtpLoadImpl.getFileName;

public class RtpCrawlImpl implements RtpCrawl {

    Set< String > links;
    Set< String > listOfAllVisitedPages;
    Set< String > listOfAllPagesToVisit;
    Set< String > listOfAllUnloadedElements;
    RtpLoadImpl rtpLoadImpl = new RtpLoadImpl();

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0";

    @Override
    public void collectAndGoThrowAllLinks(Crawler crawler) {
        System.out.println("CollectAllLinks started");
        System.out.println("Thread name: " + Thread.currentThread().getName());
        links = crawler.getLinks();
        listOfAllUnloadedElements = crawler.getListOfAllUnloadedElements();
        listOfAllVisitedPages = crawler.getListOfAllVisitedPages();
        listOfAllPagesToVisit = crawler.getListOfAllPagesToVisit();
        while (!crawler.getUrl().isEmpty()) {
            String currentUrl;
            if (listOfAllPagesToVisit.isEmpty()) {
                currentUrl = crawler.getUrl();
                listOfAllVisitedPages.add(currentUrl);
            } else {
                currentUrl = nextUrl(crawler.getUrl());
            }
            if (Jsoup.isValid(currentUrl, Safelist.basicWithImages())) {
                crawling(currentUrl);
            }
            listOfAllPagesToVisit.addAll(getLinks());
            if (!listOfAllUnloadedElements.isEmpty()) {
                listOfAllPagesToVisit.addAll(getListOfAllUnloadedElements());
            }
        }
        System.out.println("CollectAllLinks done: " + getLinks().size());
    }

    private String nextUrl(String url) {
        if (listOfAllPagesToVisit.contains(url)) {
            listOfAllPagesToVisit.remove(url);
            listOfAllVisitedPages.add(url);
        }
        return url;
    }

    @Override
    public void crawling(String url) {
        if (isValidURL(url)) {
            if (links.contains(url)) {
                return;
            }
            Document document;
            try {
                System.out.println("Thread name: " + Thread.currentThread().getName());
                document = Jsoup.connect(url).userAgent(USER_AGENT).get();
                if (document.connection().response().statusCode() == 200) {
                    links.add(url);
                    System.out.println("\n**Visiting** received web page at " + url);
                    Elements links = document.select("a[href]");
                    Elements medias = document.select("[src]");
                    Elements imports = document.select("link[href]");
                    for (Element media : medias) {
                        //System.out.println(media.attr("abs:src"));
                        rtpLoadImpl.createDirAndDownloadSrc(new Loader(media.attr("abs:src"), getFileName(media.attr("abs:src"))));
                        Printer printer = new Printer(Thread.currentThread().getName(), media.attr("abs:src"), Paths.get("").toAbsolutePath() + "/" + media.attr("abs:src"));
                        System.out.println(printer);
                    }
                    for (Element importOfSources : imports) {
                        //System.out.println(importOfSources.attr("abs:src"));
                        rtpLoadImpl.createDirAndDownloadSrc(new Loader(importOfSources.attr("abs:src"), getFileName(importOfSources.attr("abs:src"))));
                        Printer printer = new Printer(Thread.currentThread().getName(), importOfSources.attr("abs:src"), Paths.get("").toAbsolutePath() + "/" + importOfSources.attr("abs:src"));
                        System.out.println(printer);
                    }
                    for (Element link : links) {
                        String href = link.attr("abs:href");
                        //System.out.println(link.attr("abs:href"));
                        rtpLoadImpl.downloadHTMLFile(new Loader(link.attr("abs:href"), getFileName(link.attr("abs:href"))));
                        Printer printer = new Printer(Thread.currentThread().getName(), link.attr("abs:src"), Paths.get("").toAbsolutePath() + "/" + link.attr("abs:src"));
                        System.out.println(printer);
                        URL hrefUrl = new URL(href);
                        if (hrefUrl.getHost().equals(new URL(url).getHost())) { // here check we the host name is the same as we had
                            crawling(href);
                        }
                    }
                }
            } catch (MalformedURLException e) {
                System.err.println("Unsupported protocol for URL: " + url + " Error: " + e);
                saveUnloadedElement(url); //spara misslyckades URL
            } catch (IOException e) {
                System.err.println("Timeout fetching URL: " + url + " Error: " + e);
                saveUnloadedElement(url);
            }
        }
    }

    public void saveUnloadedElement(String url) {
        if (!listOfAllUnloadedElements.contains(url)) {
            listOfAllUnloadedElements.add(url);
        }
        System.out.println("Page is unloaded!");
    }

    public Set< String > getLinks() {
        return links;
    }

    public Set< String > getListOfAllUnloadedElements() {
        return listOfAllUnloadedElements;
    }

    public boolean isValidURL(String url) {
        try {
            new URL(url).toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
        return true;
    }


    public static boolean isUriAbsolute(String url) {
        try {
            URI uri = new URI(url);
            return uri.isAbsolute();
        } catch (URISyntaxException e) {
            return false;
        }
    }
}
