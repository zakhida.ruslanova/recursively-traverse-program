package se.ruza.rtp.service;

import se.ruza.rtp.model.Loader;

import java.io.IOException;
import java.nio.file.Path;

public interface RtpLoad {

    void createDirAndDownloadSrc(Loader loader);

    void createDirAndDownloadHtml(Loader loader);

    Path createDirectory(String path) throws IOException;

    void downloadHTMLFile(Loader loader);
}
