package se.ruza.rtp.model;

import se.ruza.rtp.service.RtpCrawlImpl;

public class Executor {

    private final Crawler crawler;
    private final RtpCrawlImpl rtpCrawl;
    public final static String TRETTON37 = "https://tretton37.com/";

    public Executor() {
        this.crawler = new Crawler(TRETTON37);
        this.rtpCrawl = new RtpCrawlImpl();
    }

    public Crawler getCrawler() {
        return crawler;
    }

    public RtpCrawlImpl getRtpCrawl() {
        return rtpCrawl;
    }
}
