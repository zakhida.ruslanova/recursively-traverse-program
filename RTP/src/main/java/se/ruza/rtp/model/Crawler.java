package se.ruza.rtp.model;

import java.util.HashSet;
import java.util.Set;

public class Crawler {

    private Set< String > listOfAllVisitedPages;
    private Set< String > listOfAllPagesToVisit;
    private Set< String > listOfAllUnloadedElements;
    private String url;
    private Set< String > links;

    public Crawler(String url) { // vid skapande av Crawler tar vi bara url(Skapar EN GÅNG) och listorna skapas automatiskt
        this.listOfAllVisitedPages = new HashSet<>();
        this.listOfAllPagesToVisit = new HashSet<>();
        this.listOfAllUnloadedElements = new HashSet<>();
        this.url = url;
        this.links = new HashSet<>();
    }

    public Set< String > getListOfAllVisitedPages() {
        return listOfAllVisitedPages;
    }

    public void setListOfAllVisitedPages(Set< String > listOfAllVisitedPages) {
        this.listOfAllVisitedPages = listOfAllVisitedPages;
    }

    public Set< String > getListOfAllPagesToVisit() {
        return listOfAllPagesToVisit;
    }

    public void setListOfAllPagesToVisit(Set< String > listOfAllPagesToVisit) {
        this.listOfAllPagesToVisit = listOfAllPagesToVisit;
    }

    public Set< String > getListOfAllUnloadedElements() {
        return listOfAllUnloadedElements;
    }

    public void setListOfAllUnloadedElements(Set< String > listOfAllUnloadedElements) {
        this.listOfAllUnloadedElements = listOfAllUnloadedElements;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set< String > getLinks() {
        return links;
    }

    public void setLinks(Set< String > links) {
        this.links = links;
    }
}
