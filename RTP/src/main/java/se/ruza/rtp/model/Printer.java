package se.ruza.rtp.model;

public class Printer {
    private String currentThreadName;
    private String urlOfLoadedPage;
    private String directoryOfSavedFile;

    public Printer(String currentThreadName, String urlOfLoadedPage, String directoryOfSavedFile) {
        this.currentThreadName = currentThreadName;
        this.urlOfLoadedPage = urlOfLoadedPage;
        this.directoryOfSavedFile = directoryOfSavedFile;
    }

    public String getCurrentThreadName() {
        return currentThreadName;
    }

    public void setCurrentThreadName(String currentThreadName) {
        this.currentThreadName = currentThreadName;
    }

    public String getUrlOfLoadedPage() {
        return urlOfLoadedPage;
    }

    public void setUrlOfLoadedPage(String urlOfLoadedPage) {
        this.urlOfLoadedPage = urlOfLoadedPage;
    }

    public String getDirectoryOfSavedFile() {
        return directoryOfSavedFile;
    }

    public void setDirectoryOfSavedFile(String directoryOfSavedFile) {
        this.directoryOfSavedFile = directoryOfSavedFile;
    }

    @Override
    public String toString() {
        return String.format("currentThreadName:%s, urlOfLoadedPage:%s, directoryOfSavedFile:%s", currentThreadName, urlOfLoadedPage, directoryOfSavedFile);
    }
}
