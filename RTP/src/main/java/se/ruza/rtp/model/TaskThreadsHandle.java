package se.ruza.rtp.model;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskThreadsHandle implements Runnable { // class som skapar tråder och som ska ha Crawler som parameter i sig

    Executor executor;
    ExecutorService service;// kör igång själva TaskThreadsHandle
    int amountCPU; //det beror på hur många CPU datorn har

    public TaskThreadsHandle(Executor executor) {
        this.service = Executors.newFixedThreadPool(getAmountCPU());
        this.amountCPU = getAmountCPU();
        this.executor = executor;
    }

    public ExecutorService getService() {
        return service;
    }

    public int getAmountCPU() {
        return Runtime.getRuntime().availableProcessors(); //för att få antal tillgängliga CPU
    }

    @Override
    public void run() {
        try {
            System.out.println("Thread name: " + Thread.currentThread().getName());
            executor.getRtpCrawl().collectAndGoThrowAllLinks(executor.getCrawler());
            Thread.sleep(Duration.ofSeconds(2).toMillis()); //låta andra tråder att jobba
        } catch (InterruptedException e) {
            System.err.println(e.getMessage() + e);
            Thread.currentThread().interrupt();
        }
    }
}
