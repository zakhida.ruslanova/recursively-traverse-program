package se.ruza.rtp.service;

import org.junit.Assert;
import org.junit.Test;
import se.ruza.rtp.model.Loader;

public class RtpLoadImplTest {

    private static final String TRETTON37_URL = "https://tretton37.com/";
    private static final String TRETTON37_URL_HTML_FILE = "https/tretton37.com/meet/michal-lusiak.html";
    private static final String expectedFileName = "polyfills.js";
    private static final String expectedURL = "https://tretton37.com/assets/js/lib/polyfills.js";
    private static final String expectedOnlyURLWithoutFileName = "https://tretton37.com/assets/js/lib";
    RtpLoadImpl rtpLoad = new RtpLoadImpl();

    @Test
    public void createDirectory() {
    }

    @Test
    public void downloadHTMLFile() {
    }

    @Test
    public void createDirAndDownloadSrc() {
    }

    @Test
    public void createDirAndDownloadHtml() {
    }

    @Test
    public void isFileHtml() {
        boolean isHtmlFile = rtpLoad.isFileHtml(new Loader(TRETTON37_URL_HTML_FILE, "michal-lusiak.html"));
        Assert.assertTrue(isHtmlFile);
    }

    @Test
    public void isIndexHtml() {
        boolean isIndexHtml = rtpLoad.isIndexHtml(new Loader(TRETTON37_URL, ""));
        Assert.assertTrue(isIndexHtml);
    }

    @Test
    public void getFileName() {
        String resultFileName = RtpLoadImpl.getFileName(expectedURL);
        Assert.assertEquals(resultFileName, expectedFileName);
    }

    @Test
    public void getOnlyUrlText() {
        String resultUrl = RtpLoadImpl.getOnlyUrlText(expectedURL);
        Assert.assertEquals(resultUrl, expectedOnlyURLWithoutFileName);
    }
}
